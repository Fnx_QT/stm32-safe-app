################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Application/User/copro_sync.c \
../Application/User/lock_resource.c \
../Application/User/main.c \
../Application/User/stm32mp1xx_hal_msp.c \
../Application/User/stm32mp1xx_hal_timebase_tim.c \
../Application/User/stm32mp1xx_it.c 

OBJS += \
./Application/User/copro_sync.o \
./Application/User/lock_resource.o \
./Application/User/main.o \
./Application/User/stm32mp1xx_hal_msp.o \
./Application/User/stm32mp1xx_hal_timebase_tim.o \
./Application/User/stm32mp1xx_it.o 

C_DEPS += \
./Application/User/copro_sync.d \
./Application/User/lock_resource.d \
./Application/User/main.d \
./Application/User/stm32mp1xx_hal_msp.d \
./Application/User/stm32mp1xx_hal_timebase_tim.d \
./Application/User/stm32mp1xx_it.d 


# Each subdirectory must supply rules for building sources it contributes
Application/User/%.o Application/User/%.su: ../Application/User/%.c Application/User/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DCORE_CM4 -DUSE_HAL_DRIVER -DSTM32MP157Cxx -c -I"C:/Users/Quentin/STM32CubeIDE/workspace_1.10.1/FreeRTOS_ThreadCreation/CM4/Application/Inc" -I"C:/Users/Quentin/STM32CubeIDE/workspace_1.10.1/FreeRTOS_ThreadCreation/CM4/Application/TraceAlyzer/config" -I"C:/Users/Quentin/STM32CubeIDE/workspace_1.10.1/FreeRTOS_ThreadCreation/CM4/Application/TraceAlyzer/include" -I"C:/ST/STM32Cube_FW_MP1_V1.6.0/Drivers/STM32MP1xx_HAL_Driver/Inc" -I"C:/ST/STM32Cube_FW_MP1_V1.6.0/Drivers/STM32MP1xx_HAL_Driver/Inc/Legacy" -I"C:/ST/STM32Cube_FW_MP1_V1.6.0/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F" -I"C:/ST/STM32Cube_FW_MP1_V1.6.0/Middlewares/Third_Party/FreeRTOS/Source/include" -I"C:/ST/STM32Cube_FW_MP1_V1.6.0/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS" -I"C:/ST/STM32Cube_FW_MP1_V1.6.0/Drivers/CMSIS/Device/ST/STM32MP1xx/Include" -I"C:/ST/STM32Cube_FW_MP1_V1.6.0/Drivers/CMSIS/Include" -I"C:/ST/STM32Cube_FW_MP1_V1.6.0/Drivers/BSP/STM32MP15xx_EVAL" -I"C:/ST/STM32Cube_FW_MP1_V1.6.0/Utilities/ResourcesManager" -Og -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Application-2f-User

clean-Application-2f-User:
	-$(RM) ./Application/User/copro_sync.d ./Application/User/copro_sync.o ./Application/User/copro_sync.su ./Application/User/lock_resource.d ./Application/User/lock_resource.o ./Application/User/lock_resource.su ./Application/User/main.d ./Application/User/main.o ./Application/User/main.su ./Application/User/stm32mp1xx_hal_msp.d ./Application/User/stm32mp1xx_hal_msp.o ./Application/User/stm32mp1xx_hal_msp.su ./Application/User/stm32mp1xx_hal_timebase_tim.d ./Application/User/stm32mp1xx_hal_timebase_tim.o ./Application/User/stm32mp1xx_hal_timebase_tim.su ./Application/User/stm32mp1xx_it.d ./Application/User/stm32mp1xx_it.o ./Application/User/stm32mp1xx_it.su

.PHONY: clean-Application-2f-User

