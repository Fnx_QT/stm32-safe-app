################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
S_SRCS += \
../Application/Setup/startup_stm32mp15xx.s 

OBJS += \
./Application/Setup/startup_stm32mp15xx.o 

S_DEPS += \
./Application/Setup/startup_stm32mp15xx.d 


# Each subdirectory must supply rules for building sources it contributes
Application/Setup/%.o: ../Application/Setup/%.s Application/Setup/subdir.mk
	arm-none-eabi-gcc -mcpu=cortex-m4 -g3 -c -x assembler-with-cpp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@" "$<"

clean: clean-Application-2f-Setup

clean-Application-2f-Setup:
	-$(RM) ./Application/Setup/startup_stm32mp15xx.d ./Application/Setup/startup_stm32mp15xx.o

.PHONY: clean-Application-2f-Setup

