################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Application/TraceAlyzer/trcKernelPort.c \
../Application/TraceAlyzer/trcSnapshotRecorder.c \
../Application/TraceAlyzer/trcStreamingRecorder.c 

OBJS += \
./Application/TraceAlyzer/trcKernelPort.o \
./Application/TraceAlyzer/trcSnapshotRecorder.o \
./Application/TraceAlyzer/trcStreamingRecorder.o 

C_DEPS += \
./Application/TraceAlyzer/trcKernelPort.d \
./Application/TraceAlyzer/trcSnapshotRecorder.d \
./Application/TraceAlyzer/trcStreamingRecorder.d 


# Each subdirectory must supply rules for building sources it contributes
Application/TraceAlyzer/%.o Application/TraceAlyzer/%.su: ../Application/TraceAlyzer/%.c Application/TraceAlyzer/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DCORE_CM4 -DUSE_HAL_DRIVER -DSTM32MP157Cxx -c -I"C:/Users/Quentin/STM32CubeIDE/workspace_1.10.1/FreeRTOS_ThreadCreation/CM4/Application/Inc" -I"C:/Users/Quentin/STM32CubeIDE/workspace_1.10.1/FreeRTOS_ThreadCreation/CM4/Application/TraceAlyzer/config" -I"C:/Users/Quentin/STM32CubeIDE/workspace_1.10.1/FreeRTOS_ThreadCreation/CM4/Application/TraceAlyzer/include" -I"C:/ST/STM32Cube_FW_MP1_V1.6.0/Drivers/STM32MP1xx_HAL_Driver/Inc" -I"C:/ST/STM32Cube_FW_MP1_V1.6.0/Drivers/STM32MP1xx_HAL_Driver/Inc/Legacy" -I"C:/ST/STM32Cube_FW_MP1_V1.6.0/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F" -I"C:/ST/STM32Cube_FW_MP1_V1.6.0/Middlewares/Third_Party/FreeRTOS/Source/include" -I"C:/ST/STM32Cube_FW_MP1_V1.6.0/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS" -I"C:/ST/STM32Cube_FW_MP1_V1.6.0/Drivers/CMSIS/Device/ST/STM32MP1xx/Include" -I"C:/ST/STM32Cube_FW_MP1_V1.6.0/Drivers/CMSIS/Include" -I"C:/ST/STM32Cube_FW_MP1_V1.6.0/Drivers/BSP/STM32MP15xx_EVAL" -I"C:/ST/STM32Cube_FW_MP1_V1.6.0/Utilities/ResourcesManager" -Og -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Application-2f-TraceAlyzer

clean-Application-2f-TraceAlyzer:
	-$(RM) ./Application/TraceAlyzer/trcKernelPort.d ./Application/TraceAlyzer/trcKernelPort.o ./Application/TraceAlyzer/trcKernelPort.su ./Application/TraceAlyzer/trcSnapshotRecorder.d ./Application/TraceAlyzer/trcSnapshotRecorder.o ./Application/TraceAlyzer/trcSnapshotRecorder.su ./Application/TraceAlyzer/trcStreamingRecorder.d ./Application/TraceAlyzer/trcStreamingRecorder.o ./Application/TraceAlyzer/trcStreamingRecorder.su

.PHONY: clean-Application-2f-TraceAlyzer

