################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Utilities/ResourcesManager/res_mgr.c 

OBJS += \
./Utilities/ResourcesManager/res_mgr.o 

C_DEPS += \
./Utilities/ResourcesManager/res_mgr.d 


# Each subdirectory must supply rules for building sources it contributes
Utilities/ResourcesManager/%.o Utilities/ResourcesManager/%.su: ../Utilities/ResourcesManager/%.c Utilities/ResourcesManager/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DCORE_CM4 -DUSE_HAL_DRIVER -DSTM32MP157Cxx -c -I"C:/Users/Quentin/STM32CubeIDE/workspace_1.10.1/FreeRTOS_ThreadCreation/CM4/Application/Inc" -I"C:/Users/Quentin/STM32CubeIDE/workspace_1.10.1/FreeRTOS_ThreadCreation/CM4/Application/TraceAlyzer/config" -I"C:/Users/Quentin/STM32CubeIDE/workspace_1.10.1/FreeRTOS_ThreadCreation/CM4/Application/TraceAlyzer/include" -I"C:/Users/Quentin/STM32CubeIDE/workspace_1.10.1/FreeRTOS_ThreadCreation/CM4/Drivers/STM32MP1xx_HAL_Driver/Inc" -I"C:/Users/Quentin/STM32CubeIDE/workspace_1.10.1/FreeRTOS_ThreadCreation/CM4/Drivers/STM32MP1xx_HAL_Driver/Inc/Legacy" -I"C:/Users/Quentin/STM32CubeIDE/workspace_1.10.1/FreeRTOS_ThreadCreation/CM4/Middlewares/FreeRTOS/CMSIS-RTOS/portable/GCC/ARM_CM4F" -I"C:/Users/Quentin/STM32CubeIDE/workspace_1.10.1/FreeRTOS_ThreadCreation/CM4/Middlewares/FreeRTOS/include" -I"C:/Users/Quentin/STM32CubeIDE/workspace_1.10.1/FreeRTOS_ThreadCreation/CM4/Middlewares/FreeRTOS/CMSIS-RTOS" -I"C:/Users/Quentin/STM32CubeIDE/workspace_1.10.1/FreeRTOS_ThreadCreation/CM4/Drivers/CMSIS/Device/ST/STM32MP1xx/Include" -I"C:/Users/Quentin/STM32CubeIDE/workspace_1.10.1/FreeRTOS_ThreadCreation/CM4/Drivers/CMSIS/Include" -I"C:/Users/Quentin/STM32CubeIDE/workspace_1.10.1/FreeRTOS_ThreadCreation/CM4/Drivers/BSP" -I"C:/Users/Quentin/STM32CubeIDE/workspace_1.10.1/FreeRTOS_ThreadCreation/CM4/Utilities/ResourcesManager" -Og -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Utilities-2f-ResourcesManager

clean-Utilities-2f-ResourcesManager:
	-$(RM) ./Utilities/ResourcesManager/res_mgr.d ./Utilities/ResourcesManager/res_mgr.o ./Utilities/ResourcesManager/res_mgr.su

.PHONY: clean-Utilities-2f-ResourcesManager

