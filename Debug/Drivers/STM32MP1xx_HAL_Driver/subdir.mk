################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal.c \
../Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_cortex.c \
../Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_dma.c \
../Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_dma_ex.c \
../Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_gpio.c \
../Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_hsem.c \
../Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_ipcc.c \
../Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_pwr.c \
../Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_pwr_ex.c \
../Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_rcc.c \
../Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_rcc_ex.c \
../Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_tim.c \
../Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_tim_ex.c 

OBJS += \
./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal.o \
./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_cortex.o \
./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_dma.o \
./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_dma_ex.o \
./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_gpio.o \
./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_hsem.o \
./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_ipcc.o \
./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_pwr.o \
./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_pwr_ex.o \
./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_rcc.o \
./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_rcc_ex.o \
./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_tim.o \
./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_tim_ex.o 

C_DEPS += \
./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal.d \
./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_cortex.d \
./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_dma.d \
./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_dma_ex.d \
./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_gpio.d \
./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_hsem.d \
./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_ipcc.d \
./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_pwr.d \
./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_pwr_ex.d \
./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_rcc.d \
./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_rcc_ex.d \
./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_tim.d \
./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_tim_ex.d 


# Each subdirectory must supply rules for building sources it contributes
Drivers/STM32MP1xx_HAL_Driver/%.o Drivers/STM32MP1xx_HAL_Driver/%.su: ../Drivers/STM32MP1xx_HAL_Driver/%.c Drivers/STM32MP1xx_HAL_Driver/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DCORE_CM4 -DUSE_HAL_DRIVER -DSTM32MP157Cxx -c -I"C:/Users/Quentin/STM32CubeIDE/workspace_1.10.1/FreeRTOS_ThreadCreation/CM4/Application/Inc" -I"C:/Users/Quentin/STM32CubeIDE/workspace_1.10.1/FreeRTOS_ThreadCreation/CM4/Application/TraceAlyzer/config" -I"C:/Users/Quentin/STM32CubeIDE/workspace_1.10.1/FreeRTOS_ThreadCreation/CM4/Application/TraceAlyzer/include" -I"C:/ST/STM32Cube_FW_MP1_V1.6.0/Drivers/STM32MP1xx_HAL_Driver/Inc" -I"C:/ST/STM32Cube_FW_MP1_V1.6.0/Drivers/STM32MP1xx_HAL_Driver/Inc/Legacy" -I"C:/ST/STM32Cube_FW_MP1_V1.6.0/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F" -I"C:/ST/STM32Cube_FW_MP1_V1.6.0/Middlewares/Third_Party/FreeRTOS/Source/include" -I"C:/ST/STM32Cube_FW_MP1_V1.6.0/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS" -I"C:/ST/STM32Cube_FW_MP1_V1.6.0/Drivers/CMSIS/Device/ST/STM32MP1xx/Include" -I"C:/ST/STM32Cube_FW_MP1_V1.6.0/Drivers/CMSIS/Include" -I"C:/ST/STM32Cube_FW_MP1_V1.6.0/Drivers/BSP/STM32MP15xx_EVAL" -I"C:/ST/STM32Cube_FW_MP1_V1.6.0/Utilities/ResourcesManager" -Og -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Drivers-2f-STM32MP1xx_HAL_Driver

clean-Drivers-2f-STM32MP1xx_HAL_Driver:
	-$(RM) ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal.d ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal.o ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal.su ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_cortex.d ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_cortex.o ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_cortex.su ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_dma.d ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_dma.o ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_dma.su ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_dma_ex.d ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_dma_ex.o ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_dma_ex.su ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_gpio.d ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_gpio.o ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_gpio.su ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_hsem.d ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_hsem.o ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_hsem.su ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_ipcc.d ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_ipcc.o ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_ipcc.su ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_pwr.d ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_pwr.o ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_pwr.su ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_pwr_ex.d ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_pwr_ex.o ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_pwr_ex.su ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_rcc.d ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_rcc.o ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_rcc.su ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_rcc_ex.d ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_rcc_ex.o ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_rcc_ex.su ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_tim.d ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_tim.o ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_tim.su ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_tim_ex.d ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_tim_ex.o ./Drivers/STM32MP1xx_HAL_Driver/stm32mp1xx_hal_tim_ex.su

.PHONY: clean-Drivers-2f-STM32MP1xx_HAL_Driver

