################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Middlewares/FreeRTOS/croutine.c \
../Middlewares/FreeRTOS/event_groups.c \
../Middlewares/FreeRTOS/list.c \
../Middlewares/FreeRTOS/queue.c \
../Middlewares/FreeRTOS/tasks.c \
../Middlewares/FreeRTOS/timers.c 

OBJS += \
./Middlewares/FreeRTOS/croutine.o \
./Middlewares/FreeRTOS/event_groups.o \
./Middlewares/FreeRTOS/list.o \
./Middlewares/FreeRTOS/queue.o \
./Middlewares/FreeRTOS/tasks.o \
./Middlewares/FreeRTOS/timers.o 

C_DEPS += \
./Middlewares/FreeRTOS/croutine.d \
./Middlewares/FreeRTOS/event_groups.d \
./Middlewares/FreeRTOS/list.d \
./Middlewares/FreeRTOS/queue.d \
./Middlewares/FreeRTOS/tasks.d \
./Middlewares/FreeRTOS/timers.d 


# Each subdirectory must supply rules for building sources it contributes
Middlewares/FreeRTOS/%.o Middlewares/FreeRTOS/%.su: ../Middlewares/FreeRTOS/%.c Middlewares/FreeRTOS/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DCORE_CM4 -DUSE_HAL_DRIVER -DSTM32MP157Cxx -c -I"C:/Users/Quentin/STM32CubeIDE/workspace_1.10.1/FreeRTOS_ThreadCreation/CM4/Application/Inc" -I"C:/Users/Quentin/STM32CubeIDE/workspace_1.10.1/FreeRTOS_ThreadCreation/CM4/Application/TraceAlyzer/config" -I"C:/Users/Quentin/STM32CubeIDE/workspace_1.10.1/FreeRTOS_ThreadCreation/CM4/Application/TraceAlyzer/include" -I"C:/ST/STM32Cube_FW_MP1_V1.6.0/Drivers/STM32MP1xx_HAL_Driver/Inc" -I"C:/ST/STM32Cube_FW_MP1_V1.6.0/Drivers/STM32MP1xx_HAL_Driver/Inc/Legacy" -I"C:/ST/STM32Cube_FW_MP1_V1.6.0/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F" -I"C:/ST/STM32Cube_FW_MP1_V1.6.0/Middlewares/Third_Party/FreeRTOS/Source/include" -I"C:/ST/STM32Cube_FW_MP1_V1.6.0/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS" -I"C:/ST/STM32Cube_FW_MP1_V1.6.0/Drivers/CMSIS/Device/ST/STM32MP1xx/Include" -I"C:/ST/STM32Cube_FW_MP1_V1.6.0/Drivers/CMSIS/Include" -I"C:/ST/STM32Cube_FW_MP1_V1.6.0/Drivers/BSP/STM32MP15xx_EVAL" -I"C:/ST/STM32Cube_FW_MP1_V1.6.0/Utilities/ResourcesManager" -Og -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Middlewares-2f-FreeRTOS

clean-Middlewares-2f-FreeRTOS:
	-$(RM) ./Middlewares/FreeRTOS/croutine.d ./Middlewares/FreeRTOS/croutine.o ./Middlewares/FreeRTOS/croutine.su ./Middlewares/FreeRTOS/event_groups.d ./Middlewares/FreeRTOS/event_groups.o ./Middlewares/FreeRTOS/event_groups.su ./Middlewares/FreeRTOS/list.d ./Middlewares/FreeRTOS/list.o ./Middlewares/FreeRTOS/list.su ./Middlewares/FreeRTOS/queue.d ./Middlewares/FreeRTOS/queue.o ./Middlewares/FreeRTOS/queue.su ./Middlewares/FreeRTOS/tasks.d ./Middlewares/FreeRTOS/tasks.o ./Middlewares/FreeRTOS/tasks.su ./Middlewares/FreeRTOS/timers.d ./Middlewares/FreeRTOS/timers.o ./Middlewares/FreeRTOS/timers.su

.PHONY: clean-Middlewares-2f-FreeRTOS

